﻿using System;
using System.IO;

namespace SimpleSekodeRecruitmentTest
{
	// 2# Add Basket implementation
	// Basket is a class containing list of items
	// This class has three functions:
	// 1. Sum up all prices
	// 2. Sort all items based on its price
	// 3. Print all items in basket into stream
	public class Basket
	{
		public Basket()
		{
		}

		public ulong sum()
		{
			return 0;
		}

		public void sort()
		{
		}

		public void print(Stream stream)
		{
		}
	}
}
