﻿using System;
using Newtonsoft.Json;
namespace SimpleSekodeRecruitmentTest
{
	public class Item
	{
		private String name;
		private UInt64 price;

		public Item()
		{
		}

		public string Name
		{
			get
			{
				return name;
			}

			set
			{
				name = value;
			}
		}

		public ulong Price
		{
			get
			{
				return price;
			}

			set
			{
				price = value;
			}
		}

		// Sample usage of Json.NET package - convert this object into Json
		public String toJson()
		{
			return JsonConvert.SerializeObject(this);
		}

		// 1# Utilize Json.NET to convert json into object!
		public void fromJson(String json)
		{
		}
	}
}
